import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PublicidadesComponent } from './publicidades.component';
import { PublicidadesRoutingModule } from './publicidades-routing.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { PublicidadesDetalhesComponent } from './publicidades-detalhes/publicidades-detalhes.component';
import { PublicidadesCadastroComponent } from './publicidades-cadastro/publicidades-cadastro.component';
import { NovaPublicidadeDialogComponent } from './nova-publicidade-dialog/nova-publicidade-dialog.component';
import { PublicidadesItemListagemComponent } from './publicidades-item-listagem/publicidades-item-listagem.component';

@NgModule({
  imports: [CommonModule, PublicidadesRoutingModule, SharedModule],
  declarations: [
    PublicidadesComponent,
    PublicidadesDetalhesComponent,
    PublicidadesCadastroComponent,
    NovaPublicidadeDialogComponent,
    PublicidadesItemListagemComponent
  ]
})
export class PublicidadesModule {}
