import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Router } from '@angular/router';

@Component({
  selector: 'app-nova-publicidade-dialog',
  templateUrl: './nova-publicidade-dialog.component.html',
  styleUrls: ['./nova-publicidade-dialog.component.scss']
})
export class NovaPublicidadeDialogComponent implements OnInit {
  constructor(
    public dialogRef: MatDialogRef<NovaPublicidadeDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private _router: Router
  ) {}

  ngOnInit(): void {}

  sendToHome() {
    this.dialogRef.close();
    this._router.navigate(['/publicidades']);
  }

  efetuarPagamento(dataDias: number) {
    const a = document.createElement('a');
    switch (dataDias) {
      case 10:
        a.href = 'https://pag.ae/7XqwLozsN';
        break;
      case 20:
        a.href = 'https://pag.ae/7XqwLCFG6';
        break;
      case 30:
        a.href = 'https://pag.ae/7XqwLNeVr';
        break;
      default:
        break;
    }

    a.target = '_blank';
    a.click();
  }
}
