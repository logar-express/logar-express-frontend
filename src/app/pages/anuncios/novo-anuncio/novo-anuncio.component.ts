import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { IShipping } from 'src/app/models/shipping';
import { SnackbarType } from 'src/app/models/snackbar-type.enum';
import { ShippingService } from 'src/app/services/shipping.service';
import { SnackbarService } from 'src/app/services/snackbar.service';
import { TruckerService } from 'src/app/services/trucker.service';
import { NovoAnuncioDialogComponent } from 'src/app/pages/anuncios/novo-anuncio-dialog/novo-anuncio-dialog.component';
import { EstadosService } from 'src/app/services/estados.service';

@Component({
  selector: 'app-novo-anuncio',
  templateUrl: './novo-anuncio.component.html',
  styleUrls: ['./novo-anuncio.component.scss']
})
export class NovoAnuncioComponent implements OnInit {
  form = new FormGroup({});

  acondicionamentoProdutos = [
    'Big-bad',
    'Bobinas',
    'Bombona',
    'Caixa papelão',
    'Chapas',
    'Fardos',
    'Galões',
    'Granel',
    'Outros',
    'Palete',
    'Sacaria',
    'Tambor'
  ];

  dias = [
    {
      texto: '30 dias',
      valor: 30
    },
    {
      texto: '90 dias',
      valor: 90
    },
    {
      texto: '1 ano',
      valor: 360
    }
  ];

  constructor(
    public estadosService: EstadosService,
    private _fb: FormBuilder,
    private _shippingService: ShippingService,
    public truckerService: TruckerService,
    private _snackbarService: SnackbarService,
    public dialog: MatDialog,
    private _router: Router
  ) {
    this.createForm();
  }

  ngOnInit(): void {}

  createForm() {
    this.form = this._fb.group({
      dadosFrete: this._fb.group({
        tipo_veiculo: [null, [Validators.required, Validators.maxLength(244)]],
        tipo_carreta: [null, [Validators.maxLength(244)]],
        tipo_carroceria: [null, [Validators.maxLength(244)]],
        dimensoes_minimas: [null, [Validators.maxLength(244)]],
        requisitos_extras: [null, [Validators.maxLength(244)]],
        dataDias: [null, [Validators.required]],
        possui_rastreio: [false],
        possui_movp: [false]
      }),
      produto: this._fb.group({
        tipo_produto: [null, [Validators.required, Validators.maxLength(244)]],
        acondicionamento_produto: [
          null,
          [Validators.required, Validators.maxLength(244)]
        ]
      }),
      frete: this._fb.group({
        nome_responsavel_coleta: [
          null,
          [Validators.required, Validators.maxLength(244)]
        ],
        data_coleta: [null, [Validators.required, Validators.maxLength(244)]],
        celular_coleta: [
          null,
          [Validators.required, Validators.maxLength(244)]
        ],
        valor_frete: [null, [Validators.required]],
        peso_toneladas: [null, [Validators.required]],
        endereco_coleta: [null, [Validators.maxLength(244)]],
        cidade_coleta: [null, [Validators.required, Validators.maxLength(244)]],
        estado_coleta: [null, [Validators.required, Validators.maxLength(244)]]
      }),
      entrega: this._fb.group({
        nome_responsavel_entrega: [
          null,
          [Validators.required, Validators.maxLength(244)]
        ],
        data_entrega: [null, [Validators.required, Validators.maxLength(244)]],
        celular_entrega: [
          null,
          [Validators.required, Validators.maxLength(244)]
        ],
        endereco_entrega: [null, [Validators.maxLength(244)]],
        cidade_entrega: [
          null,
          [Validators.required, Validators.maxLength(244)]
        ],
        estado_entrega: [null, [Validators.required, Validators.maxLength(244)]]
      })
    });

    this.form
      .get('dadosFrete')
      ?.get('tipo_veiculo')
      ?.valueChanges.subscribe((tipoVeiculoSelecionado: string[]) => {
        // console.log({ tipoVeiculoSelecionado });
        const tiposVeiculosSelecionado = tipoVeiculoSelecionado.join(',');
        const dadosFreteTipoCarreta = this.form
          .get('dadosFrete')
          ?.get('tipo_carreta');

        if (tiposVeiculosSelecionado.includes('Cavalo')) {
          dadosFreteTipoCarreta?.setValidators([Validators.required]);
        } else {
          dadosFreteTipoCarreta?.setValidators(null);
        }

        dadosFreteTipoCarreta?.updateValueAndValidity();
      });
  }

  submit() {
    // console.log(this.form, this.form.getRawValue());
    if (!this.form.valid) {
      console.log(this.form.errors);
      this._snackbarService.openSnackBar(
        'Fomulário inválido!',
        SnackbarType.Danger
      );

      return;
    }

    const form = Object.assign(
      this.form.get('dadosFrete')?.value,
      this.form.get('produto')?.value,
      this.form.get('frete')?.value,
      this.form.get('entrega')?.value
    );

    if (form && form.valor_frete) {
      form.valor_frete = +form.valor_frete
        .replace('R$', '')
        .replace('.', '')
        .replace(',', '.');
    }

    form.requisitos_extras = form.requisitos_extras || 'Sem requisitos';
    form.dataDias = this.dias.find((dia) => dia.texto === form.dataDias)?.valor;
    form.tipo_veiculo = form.tipo_veiculo?.join(', ');
    form.tipo_carreta = form.tipo_carreta?.join(', ');

    this._shippingService.save(form).subscribe(
      (res) => {
        this._snackbarService.openSnackBar(res.mensagem, SnackbarType.Success);

        if (res && res.frete) {
          this.dialog.open(NovoAnuncioDialogComponent, {
            data: res.frete
          });
        } else {
          this._snackbarService.openSnackBar(
            'Algo deu errado, por favor tente novamente.',
            SnackbarType.Danger
          );

          this._router.navigate(['/anuncios']);
        }
      },
      (error) => {
        const message = error.error.mensagem;

        this._snackbarService.openSnackBar(message, SnackbarType.Danger);
      }
    );
  }
}
