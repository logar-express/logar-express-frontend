import { NovoAnuncioComponent } from './novo-anuncio/novo-anuncio.component';
import { AnunciosComponent } from './anuncios.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    component: AnunciosComponent
  },
  {
    path: 'novo',
    component: NovoAnuncioComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AnunciosRoutingModule {}
