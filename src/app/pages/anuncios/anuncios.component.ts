import { AuthService } from 'src/app/services/auth.service';
import { SnackbarType } from 'src/app/models/snackbar-type.enum';
import { SnackbarService } from 'src/app/services/snackbar.service';
import { IShipping } from 'src/app/models/shipping';
import { ShippingService } from 'src/app/services/shipping.service';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-anuncios',
  templateUrl: './anuncios.component.html',
  styleUrls: ['./anuncios.component.scss']
})
export class AnunciosComponent implements OnInit {
  fretes: IShipping[] = [];

  constructor(
    private _router: Router,
    public shippingService: ShippingService,
    private _snackbarService: SnackbarService,
    public authService: AuthService
  ) {}

  ngOnInit(): void {
    this.getFretes();
  }

  newShipping() {
    this._router.navigate(['/anuncios/novo']);
  }

  getFretes() {
    this.shippingService.getAll().subscribe((res) => {
      this.fretes = res.fretes;
    });
  }

  approveShipping(id: any) {
    this.shippingService.approve(id).subscribe(
      (res: any) => {
        this._snackbarService.openSnackBar(res.mensagem, SnackbarType.Success);
      },
      (err) => {
        this._snackbarService.openSnackBar(
          err.error?.mensagem ?? err.message,
          SnackbarType.Danger
        );
        console.log('err', err);
      }
    );

    this.getFretes();
  }

  deleteShipping(id: any) {
    this.shippingService.delete(id).subscribe(
      (res: any) => {
        this._snackbarService.openSnackBar(res.mensagem, SnackbarType.Success);
      },
      (err) => {
        this._snackbarService.openSnackBar(
          err.error?.mensagem ?? err.message,
          SnackbarType.Danger
        );
        console.log('err', err);
      }
    );

    this.getFretes();
  }
}
