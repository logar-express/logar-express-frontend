import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { IShipping, RespostaListagemFretes } from 'src/app/models/shipping';
import { FavoritosService } from 'src/app/services/favoritos.service';
import { ShippingService } from 'src/app/services/shipping.service';
import { environment } from 'src/environments/environment';
import { FretesDetalhesComponent } from '../fretes/fretes-detalhes/fretes-detalhes.component';

@Component({
  selector: 'app-favoritos',
  templateUrl: './favoritos.component.html',
  styleUrls: ['./favoritos.component.scss']
})
export class FavoritosComponent implements OnInit {
  favoritos = new Set();
  fretes: IShipping[] = [];

  constructor(
    private favoritosService: FavoritosService,
    public dialog: MatDialog,
    public shippingService: ShippingService
  ) {}

  ngOnInit(): void {
    this.carregarDados();
  }

  carregarFavoritos() {
    this.favoritosService.listarFavoritos().subscribe((resposta) => {
      resposta.favoritos.forEach((favorito) => {
        this.favoritos.add(favorito.id);
      });
    });
  }

  carregarDados() {
    this.getFretes();
    this.carregarFavoritos();
  }

  removerFavorito(freteId: number) {
    this.favoritosService.removerFavorito(freteId).subscribe(() => {
      this.favoritos = new Set();
      this.fretes = [];
      this.carregarDados();
    });
  }

  adicionarFavorito(freteId: number) {
    this.favoritosService.adicionarFavorito(freteId).subscribe(() => {
      this.favoritos = new Set();
      this.fretes = [];
      this.carregarDados();
    });
  }

  openDialog(frete: IShipping) {
    const dialogRef = this.dialog.open(FretesDetalhesComponent, {
      data: { frete }
    });
  }

  getImageLink(frete: IShipping) {
    return `${environment.baseUrl}/imagem/${frete.logo}`;
  }

  getFretes() {
    this.shippingService
      .getAll()
      .subscribe((resposta) => this.formatarRespostaFrete(resposta));
  }

  formatarRespostaFrete(resposta: RespostaListagemFretes): void {
    this.fretes = resposta.fretes;
  }
}
