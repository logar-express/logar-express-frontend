import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoginService } from 'src/app/services/login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  form = new FormGroup({});

  constructor(
    private router: Router,
    private _fb: FormBuilder,
    public loginService: LoginService
  ) {
    this.createForm();
  }

  ngOnInit(): void {}

  register() {
    this.router.navigate(['/cadastro']);
  }

  login() {
    this.loginService.login(this.form.value);
  }

  createForm() {
    this.form = this._fb.group({
      email: [null, [Validators.required, Validators.email]],
      password: [null, Validators.required]
    });
  }

  recuperarSenha() {
    console.log('recuperar');
    this.router.navigate(['/recuperar-senha']);
  }
}
