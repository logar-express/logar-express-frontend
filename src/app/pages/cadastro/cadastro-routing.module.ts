import { TruckerComponent } from './trucker/trucker.component';
import { CompanyComponent } from './company/company.component';
import { CadastroComponent } from './cadastro.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    component: CadastroComponent
  },
  {
    path: 'transportadora',
    component: CompanyComponent
  },
  {
    path: 'caminhoneiro',
    component: TruckerComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CadastroRoutingModule {}
