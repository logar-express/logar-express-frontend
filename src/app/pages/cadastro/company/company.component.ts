import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { FormTypeEnum } from 'src/app/models/formType.enum';
import { SnackbarType } from 'src/app/models/snackbar-type.enum';
import { EstadosService } from 'src/app/services/estados.service';
import { ImagemService } from 'src/app/services/imagem.service';
import { RegisterService } from 'src/app/services/register.service';
import { SnackbarService } from 'src/app/services/snackbar.service';

@Component({
  selector: 'app-company',
  templateUrl: './company.component.html',
  styleUrls: ['./company.component.scss']
})
export class CompanyComponent implements OnInit {
  form = new FormGroup({});
  file?: File;
  imagemUrl = '';

  constructor(
    private _fb: FormBuilder,
    private _registerService: RegisterService,
    private _router: Router,
    private _snackbarService: SnackbarService,
    public estadosService: EstadosService,
    private imagemService: ImagemService
  ) {
    this.createForm();
  }

  ngOnInit(): void {
    this.getPersonalInfo();
  }

  getPersonalInfo() {
    this._registerService
      .getForm()
      .subscribe((val) => {
        if (!val.value.name) {
          this._router.navigate(['/cadastro']);
          return;
        }
        this.form.get('personal')?.patchValue(val.value);
      })
      .unsubscribe();
  }

  createForm() {
    this.form = this._fb.group({
      personal: this._fb.group({
        name: [null],
        password: [null],
        email: [null]
      }),
      company: this._fb.group({
        razao_social: [null, Validators.required],
        cnpj: [null, Validators.required]
      }),
      employee: this._fb.group({
        nome_funcionario: [null, Validators.required],
        cargo: [null, Validators.required],
        endereco_comercial: [null, Validators.required],
        cidade: [null, Validators.required],
        estado: [null, Validators.required],
        cep: [null, Validators.required]
      }),
      contact: this._fb.group({
        telefone: [null, Validators.required],
        celular: [null, Validators.required],
        logo: [null]
      })
    });
  }

  submit() {
    if (!this.form.valid) {
      this._snackbarService.openSnackBar(
        'Fomulário inválido!',
        SnackbarType.Danger
      );

      return;
    }

    const form = Object.assign(
      this.form.get('personal')?.value,
      this.form.get('company')?.value,
      this.form.get('employee')?.value,
      this.form.get('contact')?.value
    );

    this._registerService
      .save({ ...form, logo: this.imagemUrl }, FormTypeEnum.Transportadora)
      .subscribe(
        (res) => {
          this._snackbarService.openSnackBar(
            res.mensagem,
            SnackbarType.Success
          );

          this._router.navigate(['/login']);
        },
        (error) => {
          const message = error.error.mensagem;

          this._snackbarService.openSnackBar(message, SnackbarType.Danger);
        }
      );
  }

  onChange(event: any) {
    this.file = event?.target?.files[0];

    if (this.file && this.file.size > 1024000) {
      this._snackbarService.openSnackBar(
        'Imagem muito grande, por favor escolha uma imagem com menos de 1024KB.',
        SnackbarType.Danger
      );
      return;
    }

    if (this.file) {
      this.imagemService.uploadImagem(this.file).subscribe(
        (res) => {
          this._snackbarService.openSnackBar(
            res.mensagem,
            SnackbarType.Success
          );
          this.imagemUrl = res.uploadedImageResponse.image_name;
        },
        (err) => {
          this._snackbarService.openSnackBar(
            err.error.mensagem,
            SnackbarType.Danger
          );
        }
      );
    }
  }
}
