import { ShippingService } from 'src/app/services/shipping.service';
import { FretesDetalhesComponent } from './fretes-detalhes/fretes-detalhes.component';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import {
  FiltroFretes,
  IShipping,
  RespostaListagemFretes
} from 'src/app/models/shipping';
import { environment } from 'src/environments/environment';
import { LoaderService } from 'src/app/services/loader.service';
import { FavoritosService } from 'src/app/services/favoritos.service';
import { FormGroup, FormControl } from '@angular/forms';
import { debounceTime } from 'rxjs/operators';
import { SessionService } from 'src/app/services/session.service';
import { AuthService } from 'src/app/services/auth.service';
import { RoleEnum } from 'src/app/models/roles';

@Component({
  selector: 'app-fretes',
  templateUrl: './fretes.component.html',
  styleUrls: ['./fretes.component.scss']
})
export class FretesComponent implements OnInit, OnDestroy {
  fretes: IShipping[] = [];

  ordenacaoListagem = 0;

  opcoesOrdenacao = [
    {
      descricao: 'Ordenar por: data coleta',
      valor: 0
    },
    {
      descricao: 'Ordenar por: mais antigos',
      valor: 1
    },
    {
      descricao: 'Ordenar por: peso',
      valor: 2
    }
  ];

  carregado = false;

  opcoesCarrocerias: string[] = [];
  opcoesVeiculos: string[] = [];
  opcoesDestino: string[] = [];
  opcoesOrigem: string[] = [];

  favoritos = new Set();

  filtrado = false;

  buscarFretesForm = new FormGroup({
    cidade_coleta: new FormControl(),
    cidade_entrega: new FormControl(),
    tipo_carroceria: new FormControl(),
    tipo_veiculo: new FormControl(),
    possui_rastreio: new FormControl(),
    possui_movp: new FormControl()
  });

  isAdmin = false;

  constructor(
    public dialog: MatDialog,
    public shippingService: ShippingService,
    private loaderService: LoaderService,
    private favoritosService: FavoritosService,
    private authService: AuthService
  ) {}

  ngOnInit(): void {
    this.isAdmin = this.authService.getSession().role === RoleEnum.Admin;
    this.loaderService.setIsPesquisa(true);
    this.carregarDados();

    this.buscarFretesForm.valueChanges
      .pipe(debounceTime(2500))
      .subscribe((filtros: FiltroFretes) => this.filtrarFretes(filtros));
  }

  ngOnDestroy(): void {
    this.loaderService.setIsPesquisa(false);
  }

  carregarDados() {
    this.filtrado
      ? this.filtrarFretes(this.buscarFretesForm.getRawValue())
      : this.getFretes();
    this.getFavoritos();
  }

  getFavoritos() {
    this.favoritosService.listarFavoritos().subscribe((resposta) => {
      resposta.favoritos.forEach((favorito) => {
        this.favoritos.add(favorito.id);
      });
    });
  }

  removerFavorito(freteId: number) {
    this.favoritosService.removerFavorito(freteId).subscribe(() => {
      this.favoritos = new Set();
      // this.fretes = [];
      this.carregarDados();
    });
  }

  adicionarFavorito(freteId: number) {
    this.favoritosService.adicionarFavorito(freteId).subscribe(() => {
      this.favoritos = new Set();
      // this.fretes = [];
      this.carregarDados();
    });
  }

  reordenarListagem(): void {
    switch (this.ordenacaoListagem) {
      case 0:
        this.fretes = this.fretes.sort((freteA, freteB) => {
          if (freteA.data_coleta < freteB.data_coleta) {
            return -1;
          }
          if (freteA.data_coleta > freteB.data_coleta) {
            return 1;
          }
          return 0;
        });
        break;
      case 1:
        this.fretes = this.fretes.sort((freteA, freteB) => {
          if (freteA.created_at < freteB.created_at) {
            return -1;
          }
          if (freteA.created_at > freteB.created_at) {
            return 1;
          }
          return 0;
        });
        break;
      case 2:
        this.fretes = this.fretes.sort((freteA, freteB) => {
          if (freteA.peso_toneladas < freteB.peso_toneladas) {
            return -1;
          }
          if (freteA.peso_toneladas > freteB.peso_toneladas) {
            return 1;
          }
          return 0;
        });
        break;
      default:
        break;
    }
  }

  openDialog(frete: IShipping) {
    const dialogRef = this.dialog.open(FretesDetalhesComponent, {
      data: { frete }
    });
  }

  filtrarFretes(filtros: FiltroFretes): void {
    this.carregado = false;
    this.filtrado = true;
    this.shippingService
      .filtrarFretes(filtros)
      .subscribe((resposta) => this.formatarRespostaFrete(resposta))
      .add(() => (this.carregado = true));
  }

  getFretes() {
    this.carregado = false;
    this.shippingService
      .getAll()
      .subscribe((resposta) => this.formatarRespostaFrete(resposta))
      .add(() => (this.carregado = true));
  }

  formatarRespostaFrete(resposta: RespostaListagemFretes): void {
    this.fretes = resposta.fretes;
    this.reordenarListagem();

    this.opcoesCarrocerias = [
      ...new Set(
        resposta.carrocerias.map((carroceria) => carroceria.tipo_carroceria)
      )
    ];

    const veiculos = [
      ...new Set(
        resposta.veiculos.map((veiculo) => {
          return veiculo.tipo_veiculo.split(', ');
        })
      )
    ];

    this.opcoesVeiculos = [
      ...new Set(
        veiculos.flat(1).sort(function (a, b) {
          if (a < b) {
            return -1;
          }
          if (a > b) {
            return 1;
          }
          return 0;
        })
      )
    ];

    this.opcoesDestino = [
      ...new Set(
        resposta.destino.map(
          (destino) => `${destino.cidade_entrega} - ${destino.estado_entrega}`
        )
      )
    ];

    this.opcoesOrigem = [
      ...new Set(
        resposta.origem.map(
          (origem) => `${origem.cidade_coleta} - ${origem.estado_coleta}`
        )
      )
    ];
  }

  getImageLink(frete: IShipping) {
    return `${environment.baseUrl}/imagem/${frete.logo}`;
  }
}
