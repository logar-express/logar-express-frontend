import { FretesDetalhesComponent } from './fretes-detalhes/fretes-detalhes.component';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatSelectModule } from '@angular/material/select';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatInputModule } from '@angular/material/input';
import { NgxSpinnerModule } from 'ngx-spinner';
import { FretesRoutingModule } from './fretes-routing.module';
import { FretesComponent } from './fretes.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { FretesBuscarComponent } from './fretes-buscar/fretes-buscar.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatNativeDateModule } from '@angular/material/core';
import { TruckerService } from 'src/app/services/trucker.service';
import { MatExpansionModule } from '@angular/material/expansion';

@NgModule({
  declarations: [
    FretesComponent,
    FretesDetalhesComponent,
    FretesBuscarComponent
  ],
  imports: [
    CommonModule,
    FretesRoutingModule,
    SharedModule,
    MatSelectModule,
    MatInputModule,
    FormsModule,
    MatAutocompleteModule,
    NgxSpinnerModule,
    ReactiveFormsModule,
    MatNativeDateModule,
    MatExpansionModule
  ],
  providers: [TruckerService],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class FretesModule {}
