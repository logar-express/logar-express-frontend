import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { FiltroFretes } from 'src/app/models/shipping';
import { Observable } from 'rxjs';
import { debounceTime, map, startWith } from 'rxjs/operators';

@Component({
  selector: 'app-fretes-buscar',
  templateUrl: './fretes-buscar.component.html',
  styleUrls: ['./fretes-buscar.component.scss']
})
export class FretesBuscarComponent implements OnInit {
  @Input() opcoesCarrocerias: string[] = [];
  @Input() opcoesVeiculos: string[] = [];
  @Input() opcoesDestino: string[] = [];
  @Input() opcoesOrigem: string[] = [];

  @Input() buscarFretesForm!: FormGroup;

  @Output() filtrarFretesEvent = new EventEmitter<FiltroFretes>();

  filteredOptionsOrigem: Observable<string[]> = new Observable();
  filteredOptionsDestino: Observable<string[]> = new Observable();

  constructor() {}

  ngOnInit(): void {}

  ngOnChanges(): void {
    this.filteredOptionsOrigem = this.buscarFretesForm!.get(
      'cidade_coleta'
    )!.valueChanges.pipe(
      startWith(''),
      map((value) => {
        return this._filterOrigem(value);
      })
    );

    this.filteredOptionsDestino = this.buscarFretesForm!.get(
      'cidade_entrega'
    )!.valueChanges.pipe(
      startWith(''),
      map((value) => {
        return this._filterDestino(value);
      })
    );
  }

  filtrarFretes(filtros: FiltroFretes): void {
    this.filtrarFretesEvent.emit(filtros);
  }

  limparFiltros(): void {
    window.location.reload();
  }

  private _filterOrigem(value: string): string[] {
    const filterValue = value.toLowerCase();

    return this.opcoesOrigem.filter((option) =>
      option.toLowerCase().includes(filterValue)
    );
  }

  private _filterDestino(value: string): string[] {
    const filterValue = value.toLowerCase();

    return this.opcoesDestino.filter((option) =>
      option.toLowerCase().includes(filterValue)
    );
  }
}
