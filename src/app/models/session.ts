import { ICaminhoneiro, ITransportadora, IUser } from './user';

export interface ISession {
  access_token: string;
  token_type: string;
  expires_in: number;
  dados: ITransportadora | ICaminhoneiro;
  role: string;
}
