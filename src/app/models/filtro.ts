export interface BuscarFretes {
  possui_rastreio: boolean,
  possui_movp: boolean,
  cidade_coleta: string,
  cidade_entrega: string,
  tipo_carroceria: string,
  tipo_veiculo: string
}
