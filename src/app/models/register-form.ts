export interface ICompanyRegisterForm {
  name: string;
  email: string;
  password: string;
  cnpj: string;
  razao_social: string;
  endereco_comercial: string;
  cidade: string;
  estado: string;
  cep: string;
  telefone: string;
  celular: string;
  logo: string;
  nome_funcionario: string;
  cargo: string;
}

export interface ITruckerRegisterForm {
  email: string;
  password: string;
  nome: string;
  cpf: string;
  cnh: string;
  data_emissao_cnh: string;
  estado_expeditor_cnh: string;
  telefone: string;
  celular: string;
  possui_mopp: boolean;
  marca: string;
  modelo: string;
  cor: string;
  placa: string;
  ano: string;
  renavam: string;
  tipo_veiculo: string;
  tipo_carreta: string;
  tipo_carroceria: string;
}

export interface IAnuncianteForm {
  email: string;
  password: string;
  nome: string;
}
