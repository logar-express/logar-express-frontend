export interface RespostaUploadImagem {
  mensagem: string;
  uploadedImageResponse: {
    image_name: string;
    image_url: string;
    mime: string;
  };
}

