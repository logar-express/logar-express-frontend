import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoaderComponent } from './loader.component';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { LoaderPesquisaComponent } from './loader-pesquisa/loader-pesquisa.component';
import { LoaderGeralComponent } from './loader-geral/loader-geral.component';

@NgModule({
  declarations: [
    LoaderComponent,
    LoaderPesquisaComponent,
    LoaderGeralComponent
  ],
  imports: [CommonModule, MatProgressSpinnerModule],
  exports: [LoaderComponent]
})
export class LoaderModule {}
