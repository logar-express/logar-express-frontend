import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { IShipping } from 'src/app/models/shipping';

@Injectable({
  providedIn: 'root'
})
export class FavoritosService {
  private baseUrlFavoritos = `${environment.baseUrl}/favoritos`;

  constructor(private httpClient: HttpClient) {}

  listarFavoritos() {
    return this.httpClient.get<{ favoritos: IShipping[] }>(
      `${this.baseUrlFavoritos}`
    );
  }

  adicionarFavorito(frete_id: number) {
    return this.httpClient.post<{ favoritos: IShipping[] }>(
      `${this.baseUrlFavoritos}`,
      { frete_id }
    );
  }

  removerFavorito(frete_id: number) {
    return this.httpClient.delete<{ favoritos: IShipping[] }>(
      `${this.baseUrlFavoritos}/${frete_id}`
    );
  }
}
