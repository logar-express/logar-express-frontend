import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import {
  FiltroFretes,
  IShipping,
  RespostaListagemFretes
} from 'src/app/models/shipping';

@Injectable({
  providedIn: 'root'
})
export class ShippingService {
  api = 'frete';
  baseUrl = `${environment.baseUrl}/${this.api}`;

  approvalBody = {
    frete_aprovado: true
  };

  constructor(private http: HttpClient) {}

  save(form: IShipping) {
    return this.http.post<any>(`${this.baseUrl}`, form);
  }

  filtrarFretes(filtros: FiltroFretes) {
    return this.http.post<RespostaListagemFretes>(
      `${environment.baseUrl}/filtro-frete`,
      filtros
    );
  }

  getAll() {
    return this.http.get<RespostaListagemFretes>(`${this.baseUrl}`);
  }

  approve(id: number) {
    return this.http.put<any>(`${this.baseUrl}/${id}`, this.approvalBody);
  }

  delete(id: number) {
    return this.http.delete(`${this.baseUrl}/${id}`);
  }
}
