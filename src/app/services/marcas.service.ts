import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class MarcasService {
  constructor() {}

  public getModelos(marcaSelecionada: string): string[] {
    switch (marcaSelecionada) {
      case 'AGRALE':
        return [
          '10000 LX CUMMINS',
          '10000 LX MWM',
          '10000 S CUMMINS',
          '10000 S MWM',
          '14000 LX',
          '14000 S',
          '8700 LX',
          '8700 S',
          '8700 TR',
          'A10000',
          'A7500',
          'A8700'
        ];
        break;
      case 'CITROEN':
        return ['JUMPER FURGÃO 2.3 HDI L6', 'JUMPER FURGÃO VETRATO 2.3 HDI L6'];
        break;
      case 'DAF':
        return [
          'CF85 360 FT',
          'CF85 360 FTS',
          'CF85 410 FT',
          'CF85 410 FTS',
          'XF105 410 FTS',
          'XF105 460 FTS',
          'XF105 460 FTT',
          'XF105 510 FTS',
          'XF105 510 FTT'
        ];
        break;
      case 'FIAT':
        return [
          'DOBLÒ CARGO 1.4 FLEX',
          'DOBLÒ CARGO 1.8 FLEX',
          'DUCATO CARGO 7,5M³',
          'DUCATO CARGO L 9 M³',
          'DUCATO MAXICARGO 10 M³',
          'DUCATO MAXICARGO 12 M³',
          'FIORINO FURGÃO',
          'UNO FURGÃO 1.0'
        ];
        break;
      case 'FORD':
        return [
          'CARGO 1119',
          'CARGO 1419',
          'CARGO 1519',
          'CARGO 1719',
          'CARGO 1723',
          'CARGO 1723 KOLECTOR',
          'CARGO 1723 KOLECTOR TORQSHIFT',
          'CARGO 1723 TORQSHIFT',
          'CARGO 1729 RÍGIDO',
          'CARGO 1729 TRACTOR TORQSHIFT',
          'CARGO 1729R TORQSHIFT',
          'CARGO 1729T',
          'CARGO 1933',
          'CARGO 1933 TRACTOR TORQSHIFT',
          'CARGO 2042',
          'CARGO 2423',
          'CARGO 2429',
          'CARGO 2429 TORQSHIFT',
          'CARGO 2623',
          'CARGO 2629',
          'CARGO 2842',
          'CARGO 3129',
          'CARGO 3129 BETONEIRA',
          'CARGO 3133',
          'CARGO 816',
          'F-350',
          'F-4000'
        ];
        break;
      case 'FOTON':
        return [
          'FOTON 10 – 16DT',
          'FOTON AUMARK 3.5 – 11DT',
          'FOTON AUMARK 3.5 – 11ST',
          'FOTON AUMARK 3.5 – 14ST'
        ];
        break;
      case 'HYUNDAI':
        return ['HR 2.5'];
        break;
      case 'INTERNATIONAL':
        return ['DURASTAR', '9800I'];
        break;
      case 'IVECO':
        return [
          'DAILY 35S14',
          'DAILY 35S14 – GRAN FURGONE',
          'DAILY 40S14',
          'DAILY 45S17 – GRAN FURGONE',
          'DAILY 55C17 – GRAN FURGONE',
          'DAILY 55C17 – MAXI FURGONE',
          'DAILY ECOLINE 55C17',
          'DAILY TRUCK 70C17',
          'HI-WAY 490S44T',
          'HI-WAY 600S44T',
          'HI-WAY 600S48T',
          'HI-WAY 800S48TZ',
          'HI-WAY 800S56TZ',
          'STRALIS 460S36T',
          'STRALIS 490S40T',
          'STRALIS 490S44T',
          'STRALIS 530S36T',
          'STRALIS 600S40T',
          'STRALIS 600S44T',
          'STRALIS 800S44TZ',
          'STRALIS 800S48TZ',
          'TECTOR 150E21 ECONOMY',
          'TECTOR 170E28',
          'TECTOR 240E28',
          'TECTOR 240E28S STRADALE',
          'TECTOR 260E28',
          'TECTOR 260E30',
          'TECTOR ATTACK 170E22',
          'TECTOR ATTACK 170E28',
          'TECTOR ATTACK 240E22',
          'TRAKKER 410T44',
          'TRAKKER 410T48',
          'TRAKKER 740T44T',
          'VERTIS 130V19 HD',
          'VERTIS 90V18 HD'
        ];
        break;
      case 'KIA':
        return ['BONGO – RODEIRO SIMPLES'];
        break;
      case 'MAN':
        return ['MAN TGX 28.440', 'MAN TGX 29.440', 'MAN TGX 29.480'];
        break;
      case 'MERCEDES':
        return [
          'ACCELO 1016',
          'ACCELO 1316',
          'ACCELO 815',
          'ACTROS 2546 LS ESTRADEIRO',
          'ACTROS 2646 LS ESTRADEIRO',
          'ACTROS 2646 LS MULTIUSO',
          'ACTROS 2651 S 6X4 ESTRADEIRO',
          'ACTROS 2651 S 6×4 MULTIUSO',
          'ACTROS 4160 SLT',
          'ACTROS 4844 K 8×4',
          'ATEGO 1419',
          'ATEGO 1719',
          'ATEGO 1726',
          'ATEGO 1726 4×4',
          'ATEGO 1729 COLETOR DE LIXO',
          'ATEGO 1730 – CAVALO MECÂNICO',
          'ATEGO 2426',
          'ATEGO 2430',
          'ATRON 1319',
          'ATRON 1635',
          'ATRON 2324',
          'ATRON 2729 B 6×4',
          'ATRON 2729 K 6×4',
          'ATRON 2729 P 6×4',
          'AXOR 1933',
          'AXOR 2036 ESTRADEIRO',
          'AXOR 2036 MULTIUSO',
          'AXOR 2041 ESTRADEIRO',
          'AXOR 2041 MULTIUSO',
          'AXOR 2533',
          'AXOR 2536 ESTRADEIRO',
          'AXOR 2536 MULTIUSO',
          'AXOR 2541 ESTRADEIRO',
          'AXOR 2541 MULTIUSO',
          'AXOR 2544 ESTRADEIRO',
          'AXOR 2544 MULTIUSO',
          'AXOR 2644 ESTRADEIRO',
          'AXOR 2644 MULTIUSO',
          'AXOR 3131 B',
          'AXOR 3131 K',
          'AXOR 3131 P',
          'AXOR 3344 K',
          'AXOR 3344 P',
          'AXOR 3344 S',
          'AXOR 4144 K',
          'SPRINTER 313 CDI STREET CHASSI CABINE',
          'SPRINTER 313 CDI STREET FURGÃO',
          'SPRINTER 415 CDI',
          'SPRINTER 415 CDI – CHASSI CABINE',
          'SPRINTER 515 CDI',
          'SPRINTER 515 CDI – CHASSI CABINE',
          'VITO 111 CDI'
        ];
        break;
      case 'PEUGEOT':
        return ['PARTNER'];
        break;
      case 'RENAULT':
        return ['KANGOO', 'MASTER CHASSI', 'MASTER FURGÃO'];
        break;
      case 'SCANIA':
        return [
          'G 360 LA– STREAMLINE',
          'G 400 CA– RBP835+RP835',
          'G 400 CB– RBP835+RP835',
          'G 400 LA  – C/3º EIXO 780',
          'G 400 LA– C/3º EIXO R885',
          'G 400 LA– RP835 – STREAMLINE',
          'G 400 LA– STREAMLINE',
          'G 400 LB– RB662+R660 – STREAMLINE',
          'G 440 CA– RBP835+RP835',
          'G 440 CB– RBP835+RP835',
          'G 440 CB 8×4 – RBP835+RP835',
          'G 480 CA– RBP835+RP835',
          'G 480 CB 10×4',
          'G 480 CB– RBP835+RP835',
          'G 480 CB 8×4 – RBP835+RP835',
          'P 250 CB 6×4',
          'P 250 CB 8×4',
          'P 250 DB 4×2',
          'P 250 DB 6×2',
          'P 250 DB 8×2',
          'P 270 CB– ETANOL',
          'P 270 DB 8×2',
          'P 310 CB 6×4',
          'P 310 CB 8×4 – RBP735+RB735',
          'P 310 DB 6×2',
          'P 310 DB 8×2',
          'P 310 LA 4×2',
          'P 360 CB 6×4',
          'P 360 LA– – R780',
          'P 360 LARP835',
          'P 360 LA– /4 R885',
          'P 360 LA– R885',
          'P 360 LB– R780',
          'P 360 LB– R885',
          'P 360 LB– RB662+R660',
          'R 400 LA– C/3º EIXO R885 (STREAMLINE/HIGHLINE)',
          'R 400 LA– R780 (STREAMLINE)',
          'R 400 LA– RP835 (STREAMLINE)',
          'R 400 LA– 3º EIXO -RP835 (STREAMLINE)',
          'R 400 LA– R885 – STREAMLINE',
          'R 400 LA– R885 – STREAMLINE/HIGHLINE',
          'R 400 LA  – HIGHLINE R780',
          'R 440',
          'R 440 LA– 3º EIXO R885 – STREAMLINE',
          'R 440 LA– 3º EIXO- R885- HIGHLINE',
          'R 440 LAC/3º EIXO RP835 – HIGHLINE',
          'R 440 LA– R780 – STREAMLINE',
          'R 440 LA– R780 – STREAMLINE/HIGHLINE',
          'R 440 LA– RP835 – HIGHLINE',
          'R 440 LA– RP835 – STREAMLINE',
          'R 440 LA– R885 – STREAMLINE',
          'R 440 LA– R885 – STREAMLINE/HIGHLINE',
          'R 440 LA– RBP835+RP835- STREAMLINE',
          'R 440 LA– RBP835+RP835- STREAMLINE/HIGHLINE',
          'R 440 LA 6X4 – RB662+R660 – STREAMLINE',
          'R 440 LA 6X4 – RB662+R660 – STREAMLINE/HIGHLINE',
          'R 480 LA– C/3º EIXO R885 – HIGHLINE',
          'R 480 LA– C/3º EIXO R885 – STREAMLINE',
          'R 480 LA– C/3º EIXO RP835 – HIGHLINE',
          'R 480 LA– C/3º EIXO RP835 – STREAMLINE',
          'R 480 LA– R780 – STREAMLINE',
          'R 480 LA– R780 – STREAMLINE/HIGHLINE',
          'R 480 LA– RP835 – STREAMLINE',
          'R 480 LA– R885 – STREAMLINE',
          'R 480 LA– R885 – STREAMLINE/HIGHLINE',
          'R 480 LA– RB662+R660 – STREAMLINE',
          'R 480 LA– RB662+R660 – STREAMLINE/HIGHLINE',
          'R 480 LA– RBP835+RP835 – STREAMLINE',
          'R 480 LA– RBP835+RP835 – STREAMLINE/HIGHLINE',
          'R 560 LA– R885 V8 – STREAMLINE',
          'R 560 LA– R885 V8 – STREAMLINE/ HIGHLINE',
          'R 560 LA– RB662+R660 V8 – STREAMLINE',
          'R 560 LA– RB662+R660 V8 – STREAMLINE/HIGHLINE',
          'R 560 LA– RBP835+RP835 V8 – STREAMLINE',
          'R 560 LA– RBP835+RP835 V8 – STREAMLINE/HIGHLINE',
          'R 620 8×4',
          'R 620 LA– R885 V8 - STREAMLINE',
          'R 620 LA– R885 V8 - STREAMLINE/HIGHLINE',
          'R 620 LA– RB662+R660 V8 – STREAMLINE',
          'R 620 LA– RB662+R660 V8 – STREAMLINE/HIGHLINE',
          'R 620 LA– RBP835+RP835 V8 – STREAMLINE',
          'R 620 LA– RBP835+RP835 V8 – STREAMLINE/HIGHLINE'
        ];
        break;
      case 'SINOTRUK1':
        return ['A7 380', 'A7 420', 'A7 460'];
        break;
      case 'VOLKSWAGEN':
        return [
          'CONSTELLATION 13.190',
          'CONSTELLATION 15.190',
          'CONSTELLATION 17.190 V-TRONIC',
          'CONSTELLATION 17.280',
          'CONSTELLATION 17.280 V-TRONIC',
          'CONSTELLATION 17.330',
          'CONSTELLATION 19.330',
          'CONSTELLATION 19.360',
          'CONSTELLATION 19.390',
          'CONSTELLATION 19.420 V-TRONIC',
          'CONSTELLATION 24.280',
          'CONSTELLATION 24.280 8×2',
          'CONSTELLATION 24.280 V-TRONIC 6×2',
          'CONSTELLATION 24.280 V-TRONIC 8×2',
          'CONSTELLATION 24.330 6×2',
          'CONSTELLATION 24.330 8×2',
          'CONSTELLATION 25.360',
          'CONSTELLATION 25.390',
          'CONSTELLATION 25.420 V-TRONIC',
          'CONSTELLATION 26.280',
          'CONSTELLATION 26.280 8×4',
          'CONSTELLATION 26.390',
          'CONSTELLATION 26.420 V-TRONIC',
          'CONSTELLATION 31.280 6×4',
          'CONSTELLATION 31.280 8×4',
          'CONSTELLATION 31.330 10×4',
          'CONSTELLATION 31.330 6×4',
          'CONSTELLATION 31.330 8×4',
          'CONSTELLATION 31.390 10X4',
          'CONSTELLATION 31.390 6×4',
          'CONSTELLATION 31.390 8×4',
          'DELIVERY 10.160',
          'DELIVERY 10.160 PLUS',
          'DELIVERY 5.150',
          'DELIVERY 8.160',
          'DELIVERY 8.160 COM DIFF LOCK',
          'DELIVERY 9.160',
          'WORKER 13.190',
          'WORKER 15.190',
          'WORKER 17.190',
          'WORKER 17.230',
          'WORKER 23.230'
        ];
        break;
      case 'VOLVO':
        return [
          'FH 13 420',
          'FH 13 420 T',
          'FH 13 460 T',
          'FH 13 540 T',
          'FH 16 750',
          'FM 11 380 R',
          'FM 13 380 T',
          'FMX 13 380 R',
          'FMX 13 420 R',
          'FMX 13 420 T',
          'FMX 13 460 R',
          'VM 220 R',
          'VM 270 R',
          'VM 330 T',
          'VM 330 R'
        ];
        break;

      default:
        return [];
        break;
    }
  }
}
