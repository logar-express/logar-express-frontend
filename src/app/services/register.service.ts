import { FormTypeEnum } from 'src/app/models/formType.enum';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
import { Injectable } from '@angular/core';
import { Form, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { IRegisterResponse } from 'src/app/models/responses';
import {
  IAnuncianteForm,
  ICompanyRegisterForm,
  ITruckerRegisterForm
} from 'src/app/models/register-form';

@Injectable({
  providedIn: 'root'
})
export class RegisterService {
  baseUrl = `${environment.baseUrl}`;

  private form = new BehaviorSubject<FormGroup>(new FormGroup({}));

  constructor(private http: HttpClient) {}

  getForm() {
    return this.form.asObservable();
  }

  setForm(registerForm: FormGroup) {
    this.form.next(new FormGroup({}));
    this.form.next(registerForm);
  }

  save(form: ICompanyRegisterForm | ITruckerRegisterForm, formType: number) {
    const url =
      formType === FormTypeEnum.Transportadora
        ? `${this.baseUrl}/transportadora`
        : `${this.baseUrl}/caminhoneiro`;

    return this.http.post<IRegisterResponse>(url, form);
  }

  cadastrarAnunciante(form: IAnuncianteForm) {
    return this.http.post<IRegisterResponse>(`${this.baseUrl}/auth/cadastro`, form);
  }
}
