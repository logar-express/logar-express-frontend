import { RoleEnum } from 'src/app/models/roles';
import { AuthService } from 'src/app/services/auth.service';
import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  CanActivate,
  Router,
  RouterStateSnapshot,
  UrlTree
} from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LoginGuard implements CanActivate {
  constructor(private _authService: AuthService, private _router: Router) {}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ):
    | Observable<boolean | UrlTree>
    | Promise<boolean | UrlTree>
    | boolean
    | UrlTree {
    const isAuth = this._authService.isAuthenticated();

    if (!isAuth) {
      return true;
    } else {
      const role = this._authService.getRole();

      role === RoleEnum.Transportadora
        ? this._router.navigate(['/anuncios'])
        : this._router.navigate(['/fretes']);
      return false;
    }
  }
}
