import { RoleEnum } from 'src/app/models/roles';
import { AuthService } from 'src/app/services/auth.service';
import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  CanActivate,
  Router,
  RouterStateSnapshot,
  UrlTree
} from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AnuncianteGuard implements CanActivate {
  constructor(private _authService: AuthService, private _router: Router) {}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ):
    | Observable<boolean | UrlTree>
    | Promise<boolean | UrlTree>
    | boolean
    | UrlTree {
    const isAuth = this._authService.isAuthenticated();

    if (!isAuth) {
      return false;
    } else {
      const role = this._authService.getRole();

      role === RoleEnum.Anunciante
        ? this._router.navigate(['/publicidades'])
        : this._router.navigate(['/login']);
      return true;
    }
  }
}
