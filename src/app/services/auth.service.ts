import { SessionService } from './session.service';
import { ISession } from 'src/app/models/session';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  constructor(private _sessionService: SessionService) {}

  getSession() {
    const local = localStorage.getItem(`${environment.prefix}_session`);
    const session = sessionStorage.getItem(`${environment.prefix}_session`);

    return local
      ? (JSON.parse(local) as ISession)
      : (JSON.parse(session as string) as ISession);
  }

  getRole() {
    const session = this.getSession();

    return session && session.role ? session.role : '';
  }

  isAuthenticated(): boolean {
    const sessao = this.getSession();

    if (sessao && sessao.expires_in) {
      const expires = sessao.expires_in;
      const now = Date.now();

      if (expires > now) {
        return true;
      }
    }

    this._sessionService.reset();

    return false;
  }

  getToken() {
    const session = this.getSession();

    if (session && session.access_token) {
      return session.access_token;
    } else {
      return null;
    }
  }
}
