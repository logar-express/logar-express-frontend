import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { IAnuncio, IAnuncioForm } from '../models/anuncio.interface';
import { SnackbarType } from '../models/snackbar-type.enum';
import { SnackbarService } from './snackbar.service';

@Injectable({
  providedIn: 'root'
})
export class AnunciosService {
  private baseUrlPublicidade = `${environment.baseUrl}/publicidade`;

  constructor(
    private httpClient: HttpClient,
    private snackBarService: SnackbarService
  ) {}

  listarTodosAnuncios() {
    return this.httpClient.get<{ publicidades: IAnuncio[] }>(
      this.baseUrlPublicidade
    );
  }

  listarAnunciosVip() {
    return this.httpClient.get<{ publicidades: IAnuncio[] }>(
      `${this.baseUrlPublicidade}-vip`
    );
  }

  adicionarAnuncio(anuncio: IAnuncioForm) {
    return this.httpClient.post<{ mensagem: string; publicidade: IAnuncio }>(
      this.baseUrlPublicidade,
      anuncio
    );
  }

  aprovarAnuncio(anuncioId: number) {
    this.httpClient
      .put<{ mensagem: string }>(`${this.baseUrlPublicidade}/${anuncioId}`, {
        publicidade_aprovada: true
      })
      .subscribe(
        (res) => {
          this.snackBarService.openSnackBar(res.mensagem, SnackbarType.Success);
          setTimeout(() => {
            location.reload();
          }, 1000);
        },
        (err) => {
          this.snackBarService.openSnackBar(
            err.error.mensagem,
            SnackbarType.Danger
          );
        }
      );
  }

  deletarAnuncio(anuncioId: number) {
    this.httpClient
      .delete<{ mensagem: string }>(`${this.baseUrlPublicidade}/${anuncioId}`)
      .subscribe(
        (res) => {
          this.snackBarService.openSnackBar(res.mensagem, SnackbarType.Success);
          setTimeout(() => {
            location.reload();
          }, 1000);
        },
        (err) => {
          this.snackBarService.openSnackBar(
            err.error.mensagem,
            SnackbarType.Danger
          );
        }
      );
  }
}
