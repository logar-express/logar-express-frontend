import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class TruckerService {
  constructor() {}

  get tipoVeiculo() {
    return [
      '3/4',
      'Caminhão 4x2 (Toco)',
      'Caminhão 6x2 (Truck)',
      'Caminhão 6x4 (Truck traçado)',
      'Caminhão 8x2 (Bitruck)',
      'Caminhão 8x4 (Bitruck traçado)',
      'Cavalo 4x2 (Toco)',
      'Cavalo 6x2 (Truck)',
      'Cavalo 6x4 (Truck traçado)',
      'Cavalo 8x2 (Bitruck)',
      'Cavalo 8x4 (Bitruck traçado)',
      'VLC'
    ];
  }

  get tipoCarreta() {
    return [
      'Bitrem 7 eixos',
      'Bitrem 9 eixos',
      'Cangurú',
      'Eixos 2',
      'Eixos 3',
      'Rodotrem',
      'Vanderleia'
    ];
  }

  get tipoCarroceria() {
    return [
      'Báu frigorifico',
      'Báu rebaixado',
      'Báu',
      'Cegonheiro',
      'Gaiola',
      'Munk',
      'Porta container',
      'Sem carreta',
      'Sider rebaixado',
      'Sider',
      'Silo(Cebolão)',
      'Tanque',
      'cavaqueira',
      'caçamba',
      'grade baixa',
      'graneleiro'
    ];
  }
}
