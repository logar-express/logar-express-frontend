import { LoaderService } from './loader.service';
import { SnackbarService } from './snackbar.service';
import { AuthService } from './auth.service';
import { SessionService } from './session.service';
import { Injectable } from '@angular/core';
import {
  HttpEvent,
  HttpInterceptor,
  HttpHandler,
  HttpRequest,
  HttpHeaders,
  HttpResponse,
  HttpErrorResponse
} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, finalize, map } from 'rxjs/operators';

@Injectable()
export class Interceptor implements HttpInterceptor {
  constructor(
    private _authService: AuthService,
    private loaderService: LoaderService,
    private sessionService: SessionService
  ) {}

  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    const id = this.loaderService.show();

    const token = this._authService.getToken();
    let req = request.clone();

    if (token) {
      req = request.clone({
        setHeaders: {
          Authorization: `Bearer ${token}`
        }
      });
    }

    return next.handle(req).pipe(
      map((event: HttpEvent<any>) => {
        if (event instanceof HttpResponse) {
          // console.log('event--->>>', event);
        }
        return event;
      }),
      catchError((error: HttpErrorResponse) => {
        let data = {};
        data = {
          reason:
            error && error.error && error.error.reason
              ? error.error.reason
              : '',
          status: error.status
        };

        if (error.status === 401) {
          this.sessionService.logout();
        }

        return throwError(error);
      }),
      finalize(() => {
        this.loaderService.hide(id)
      })
    );
  }
}
