import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';


@Injectable({
  providedIn: 'root'
})
export class FiltroService {
  api = 'filtro-frete';
  baseUrl = `${environment.baseUrl}/${this.api}`;

  constructor(private http: HttpClient) { }

  origem = [
    {
      "cidade_coleta": "Cidade Colela 1",
      "estado_coleta": "SP"
    },
    {
      "cidade_coleta": "Cidade Colela 1",
      "estado_coleta": "SP"
    },
    {
      "cidade_coleta": "Cidade Colela",
      "estado_coleta": "SP"
    },
    {
      "cidade_coleta": "Cidade Colela",
      "estado_coleta": "SP"
    },
    {
      "cidade_coleta": "Cidade Colela",
      "estado_coleta": "SP"
    }
  ]
  destino = [
    {
      "cidade_entrega": "Cidade Entrega",
      "estado_entrega": "SP"
    },
    {
      "cidade_entrega": "Cidade Entrega",
      "estado_entrega": "SP"
    },
    {
      "cidade_entrega": "Cidade Entrega 2",
      "estado_entrega": "SP"
    },
    {
      "cidade_entrega": "Cidade Entrega 1",
      "estado_entrega": "SP"
    },
    {
      "cidade_entrega": "Cidade Entrega 1",
      "estado_entrega": "SP"
    }
  ]
  carroceria = [
    {
      "tipo_carroceria": "Carroceria B "
    },
    {
      "tipo_carroceria": "Carroceria B "
    },
    {
      "tipo_carroceria": "Carroceria A "
    },
    {
      "tipo_carroceria": "Carroceria A "
    },
    {
      "tipo_carroceria": "Carroceria A "
    }
  ]
  veiculos = [
    {
      "tipo_veiculos": "Veiculos B "
    },
    {
      "tipo_veiculos": "Veiculos B "
    },
    {
      "tipo_veiculos": "Veiculos A "
    },
    {
      "tipo_veiculos": "Veiculos A "
    },
    {
      "tipo_veiculos": "Veiculos A "
    }
  ]

}
