import { Router } from '@angular/router';
import { LoaderService } from 'src/app/services/loader.service';
import { ChangeDetectorRef, Component } from '@angular/core';
import { Subject } from 'rxjs';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'core-root',
  templateUrl: './core.component.html',
  styleUrls: ['./core.component.scss']
})
export class CoreComponent {
  isLoading: Subject<boolean> = this.loaderService.isLoading;

  constructor(
    public authService: AuthService,
    public loaderService: LoaderService,
    private cdr: ChangeDetectorRef,
    private router: Router
  ) {
    router.events.subscribe(() => {
      this.cdr.detectChanges();
    });
  }
}
