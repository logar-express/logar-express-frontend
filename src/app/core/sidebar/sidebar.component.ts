import { AuthService } from 'src/app/services/auth.service';
import { NavigationEnd, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { SessionService } from 'src/app/services/session.service';
import { RoleEnum } from 'src/app/models/roles';

interface IMenuItem {
  title: string;
  icon: string;
  link: string;
  roles: string[];
}

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {
  showSidebar = true;

  menuItems: IMenuItem[] = [
    {
      title: 'Ver Fretes',
      icon: '/assets/icons/icone-fretes-novo.png',
      link: '/fretes',
      roles: [RoleEnum.Caminhoneiro, RoleEnum.Admin]
    },
    {
      title: 'Gerenciar Anuncios',
      icon: '/assets/icons/menu-publicidade.svg',
      link: '/publicidades',
      roles: [RoleEnum.Anunciante, RoleEnum.Admin]
    },
    {
      title: 'Mercado do Caminhoneiro',
      icon: '/assets/icons/icone-mercado-novo.png',
      link: '/publicidades',
      roles: [RoleEnum.Caminhoneiro]
    },
    {
      title: 'Meus Fretes Favoritos',
      icon: '/assets/icons/heart-icon-white.png',
      link: '/favoritos',
      roles: [RoleEnum.Caminhoneiro]
    },
    {
      title: 'Gerenciar Anúncios de Frete',
      icon: '/assets/icons/menu_anuncios.svg',
      link: '/anuncios',
      roles: [RoleEnum.Transportadora, RoleEnum.Admin]
    }
  ];

  url = '';

  constructor(
    private _router: Router,
    public sessionService: SessionService,
    public authService: AuthService
  ) {}

  ngOnInit(): void {
    if (window.innerWidth < 700) {
      this.showSidebar = false;
    }

    this._router.events.subscribe((res) => {
      if (res instanceof NavigationEnd) {
        this.url = res.url;
      }
    });
  }
}
