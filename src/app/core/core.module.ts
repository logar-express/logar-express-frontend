import { SharedModule } from 'src/app/shared/shared.module';
import { LOCALE_ID, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { CoreRoutingModule } from './core-routing.module';
import { CoreComponent } from './core.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgxMaskModule } from 'ngx-mask';
import { HttpClientModule } from '@angular/common/http';
import { SidebarComponent } from './sidebar/sidebar.component';
import { InterceptorModule } from 'src/app/services/interceptor.module';
import { registerLocaleData } from '@angular/common';
import localept from '@angular/common/locales/pt';

registerLocaleData(localept, 'pt');

@NgModule({
  declarations: [CoreComponent, SidebarComponent],
  imports: [
    BrowserModule,
    CoreRoutingModule,
    BrowserAnimationsModule,
    NgxMaskModule.forRoot(),
    HttpClientModule,
    SharedModule,
    InterceptorModule
  ],
  providers: [
    {
      provide: LOCALE_ID,
      useValue: 'pt'
    }
  ],
  bootstrap: [CoreComponent]
})
export class CoreModule {}
